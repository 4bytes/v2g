#!/usr/bin/env python
# coding: utf-8

import logging
import selectors
import socket
import struct
import sys
import traceback
import ipaddress
from dataclasses import dataclass


SDP_ADDR = "ff02::1"
SDP_PORT = 15118
SDP_REQUESTS = 50

logging.basicConfig(
    format="%(asctime)s %(levelname)s:[%(name)s]: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)


# TODO:
@dataclass
class V2GFrame:
    version:   int = 0x1             # V2GTP version 1
    inversion: int = version ^ 0xff  # 0xfe: V2GTP version 1
    pl_type:   int = 0x9000          # SDP request
    pl_len:    int = None
    security:  int = 0x10            # 0x00 - TLS, 0x10 - no security
    proto_t:   int = 0x00            # TCP
    payload:   int = None


class V2GTPClient:
    def __init__(self, sdp_addr, sdp_port):
        self.log = logging.getLogger("EVCC client")
        self.sdp_addr = sdp_addr
        self.sdp_port = sdp_port
        self.secc_addr = None
        self.secc_port = None
        self.sock = None
        self.selector = selectors.DefaultSelector()
        self.events = selectors.EVENT_WRITE | selectors.EVENT_READ
        self.v2g_request = "V2G REQUEST"

    def parse_sdp_response(self, data: bytes):
        self.log.debug(f"Verify SDP response: {data}")
        secc_addr = ""
        secc_port = 0

        # heaer 8 header + 20 bytes payload
        if len(data) < 28:
            self.log.debug(f"Incorrect SDP response length: {len(data)}")
            return False
        else:
            sdp_header = struct.unpack(">BBHI", data[:8])
            if sdp_header[0] != 0x1:
                self.log.debug(f"Incorrect protocol vertion: {sdp_header[0]}")
                return False
            if sdp_header[1] ^ 0xff != sdp_header[0]:
                self.log.debug(
                    f"Incorrect inverse protocol vertion: {sdp_header[1]}"
                )
                return False
            if sdp_header[2] != 0x9001:
                self.log.debug(f"Incorrect payload type: {sdp_header[2]}")

        sdp_payload = data[8:]
        sdp_len = len(sdp_payload)
        if sdp_len != 20:
            self.log.debug(
                f"Incorrect payload length: {sdp_len}: {sdp_payload}"
            )
            return False

        try:
            secc_addr = ipaddress.ip_address(sdp_payload[:16])
        except ValueError:
            self.log.debug(f"Incerrect SECC IP address: {secc_addr}")
            return False

        secc_port = int.from_bytes(sdp_payload[16:18], byteorder='big')
        security, transport = struct.unpack(">BB", sdp_payload[18:])

        if secc_port < 49152 or secc_port > 65535:
            self.log.debug(f"Incorrect SECC port: {secc_port}")
            return False
        if security != 0x10:
            self.log.debug(f"Incorrect security type: {security}")
            return False
        if transport != 0x00:
            self.log.debug(f"Incorrect transport protocol: {transport}")
            return False
        return (str(secc_addr), secc_port)

    def make_sdp_handshake(self) -> bytes:
        sdp_payload = (0x10, 0x00)  # no security, TCP
        sdp_msg = struct.pack(
            ">BBHIBB",
            0x01,
            0xfe,
            0x9000,
            len(sdp_payload),
            *sdp_payload,
        )

        # self.log.debug(f"Send SDP request num: {n + 1}")
        self.sock.sendto(sdp_msg, (self.sdp_addr, self.sdp_port))

        # try:
        #     data, addr = self.sock.recvfrom(1024)
        #     msg = data
        # except socket.timeout:
        #     self.log.critical("Recv timeout: no response from server")

        # self.close()
        # return msg

    def start_sdp_discovery(self, mask) -> None:
        self.log.debug("Start SDP discovery")
        # traceback.print_stack()
        # if data := self.make_sdp_handshake():

        data = None
        secc_data = None
        try:
            data = self.sock.recv(1024)
            secc_data = self.parse_sdp_response(data)
        except BlockingIOError:  # socket is not ready
            pass

        if secc_data:
            self.secc_addr = secc_data[0]
            self.secc_port = secc_data[1]

            print(f"SEC DATA: {secc_data=}")

            # close and unregister UDP socket
            self.log.debug(f"Unregister and close socket: {self.sock}")
            self.close()

            self.log.debug("Create TCP socket")
            self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
            self.sock.setblocking(False)
            self.log.debug(f"Connect to: {secc_data}")

            # self.sock.connect_ex(secc_data)
            self.sock.connect_ex(("localhost", self.secc_port))

            self.selector.register(
                self.sock, selectors.EVENT_WRITE, data=self.process_v2tp_events
            )

    def process_v2tp_events(self, mask):
        if mask & selectors.EVENT_READ:
            self.v2g_read()
        if mask & selectors.EVENT_WRITE:
            self.v2g_write()

    def v2g_read(self):
        self.log.debug("V2G READ")
        # data = self.sock.recv(1024)
        # print(f"v2g_read: {data=}")

    def v2g_write(self):
        self.log.debug("V2G WRITE")
        self.sock.send(b"V2G WRITE")

    def start_connection(self) -> None:
        failed_requests = 0
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.setsockopt(
            socket.IPPROTO_IPV6,
            socket.IPV6_MULTICAST_LOOP,
            True
        )
        self.sock.setsockopt(
            socket.IPPROTO_IPV6,
            socket.IPV6_MULTICAST_HOPS,
            True
        )
        self.sock.bind(('::', 0))
        self.sock.setblocking(False)
        self.sock.connect_ex((self.sdp_addr, self.sdp_port))
        sdp_request = self.start_sdp_discovery
        self.selector.register(self.sock, self.events, data=sdp_request)

        try:
            self.make_sdp_handshake()
            while True:
                events = self.selector.select(timeout=0.025)
                if not events:
                    failed_requests += 1

                if failed_requests == SDP_REQUESTS:
                    self.log.debug("Discovery attempts exceed")
                    break

                for key, mask in events:
                    handler = key.data
                    try:
                        handler(mask)
                    except Exception:
                        print(
                            f"Main: Error: Exception for {handler}:\n"
                            f"{traceback.format_exc()}"
                        )
                if not self.selector.get_map():
                    break

        except KeyboardInterrupt:
            print("Caught keyboard interrupt, exiting")
        finally:
            self.selector.close()

    def close(self):
        self.log.debug(f"Close socket: {self.sock}")
        try:
            self.selector.unregister(self.sock)
        except Exception as err:
            self.log.error(
                f"Error: selector.unregister(): {self.sock}: {err!r}"
            )
        try:
            self.sock.close()
        except OSError as err:
            self.log.error(f"Error: socket.close():{self.sock}: {err!r}")
        finally:
            self.sock = None


if __name__ == "__main__":
    client = V2GTPClient(SDP_ADDR, SDP_PORT)
    client.start_connection()
