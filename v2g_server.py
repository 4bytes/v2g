#!/usr/bin/env python
# coding: utf-8

# TODO:
# 1. Use dataclasses for V2GTP packets
# 2. Make sdplib.py with common parts decomposition
# 3. Use FIONREAD for fragmented packets


import ipaddress
import itertools
import logging
import selectors
import socket
import struct
import sys
import ctypes
import fcntl
import termios

SDP_ADDR = "ff02::1"
SDP_PORT = 15118
ADDR = "::1"
PORT = 49999

logging.basicConfig(
    format="%(asctime)s %(levelname)s:[%(name)s]: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)


class V2GTPServer:
    def __init__(self, sdp_addr, sdp_port, addr, port):
        self.sdp_addr = sdp_addr
        self.sdp_port = sdp_port
        self.addr = addr
        self.port = port
        self.log = logging.getLogger("SECC server")
        self.sdp_sock = self.setup_sdp_socket()
        self.secc_sock = self.setup_secc_sock()
        self.selector = selectors.DefaultSelector()
        self.selector.register(
            self.sdp_sock, selectors.EVENT_READ, data=self.process_sdp_request
        )
        self.selector.register(
            self.secc_sock, selectors.EVENT_READ, data=self.v2gtp_handler
        )
        self.v2g_response = "V2G RESPONSE"
        # TODO: Do we need to track peers?
        self.peers = {}

    @property
    def ipv6addr(self):
        # assume that only one IPv6 addr is assigned to the interface
        hostname = socket.gethostname()
        addr_list = socket.getaddrinfo(hostname, None, socket.AF_INET6, 1, 0)
        return addr_list[0][4][0]

    def setup_sdp_socket(self):
        self.log.debug("Initialize IPv6 datagram (UDP) socket")
        sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_LOOP, True)
        sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, True)
        sock.bind(("", self.sdp_port))
        mcast_req = struct.pack(
            "16s15s".encode("utf-8"),
            socket.inet_pton(socket.AF_INET6, self.sdp_addr),
            (chr(0) * 16).encode("utf-8"),
        )
        sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mcast_req)
        sock.setblocking(False)
        return sock

    def setup_secc_sock(self):
        self.log.debug("Initialize IPv6 stream (TCP )socket")
        sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        sock.bind((self.addr, self.port))
        sock.listen()
        self.log.debug(f"Listening on {self.addr}:{self.port}")
        sock.setblocking(False)
        return sock

    def verify_sdp_req_msg(self, message):
        # 8 bites header, 2 bytes payload
        if len(message) < 10:
            return False

        v2g_msg_header = struct.unpack(">BBHI", message[:8])

        # protocol version
        if v2g_msg_header[0] != 0x1:
            self.log.debug(f"Incorrect protocol version: {hex(v2g_msg_header[0])}")
            return False
        # protocol inverse version
        if v2g_msg_header[1] ^ 0xff != v2g_msg_header[0]:
            self.log.debug(
                f"Incorrect inverse protocol version: {hex(v2g_msg_header[1])}"
            )
            return False
        # payload type
        if v2g_msg_header[2] != 0x9000:
            self.log.debug(f"Incorrect payload type: {hex(v2g_msg_header[1])}")
            return False

        plen = v2g_msg_header[3]
        if len(message[8:]) != plen:
            self.log.debug(f"Incorrect payload length: {len(message[8:])}")

        v2g_msg = struct.unpack(">BB", message[8:])
        security = v2g_msg[0]
        protocol = v2g_msg[1]

        self.log.debug(
            "\n----------------------------------------\n"
            "The SDP request message is correct: \n"
            f"Protocol version: {hex(v2g_msg_header[0])}\n"
            f"Inverse protocol version: {hex(v2g_msg_header[1])}\n"
            f"Payload type: {hex(v2g_msg_header[2])}\n"
            f"Payload length: {hex(v2g_msg_header[3])}\n"
            f"Security: {hex(security)}\n"
            f"Transport protocol: {hex(protocol)}"
            "\n----------------------------------------"
        )
        return True

    def format_sdp_resp_msg(self):
        self.log.debug("Format SDP response")
        payload = bytes()
        payload += ipaddress.ip_address(self.ipv6addr).packed
        # security, transport protocol
        payload += struct.pack(">HBB", self.port, 0x10, 0x00)
        # version, inverse version, response and 20 byte length payload
        v2g_header = (0x1, 0xFE, 0x9001, 0x14)
        message = struct.pack(">BBHI", *v2g_header) + payload
        self.log.debug(f"Send message: {message}")
        return message

    def fionread(self, sock: socket.socket) -> int:
        ret = fcntl.ioctl(
            sock.fileno(),
            termios.FIONREAD,
            bytes(ctypes.sizeof(ctypes.c_int))
        )
        return int.from_bytes(ret, sys.byteorder)

    def process_sdp_request(self, sock: socket.socket, mask: int):
        buf = b""
        while buf_len := self.fionread(sock):
            try:
                data, addr = sock.recvfrom(buf_len)
                buf += data
            except BlockingIOError:
                pass

        if self.verify_sdp_req_msg(buf):
            response = self.format_sdp_resp_msg()
            group = socket.inet_pton(socket.AF_INET6, self.sdp_addr)
            sock.sendto(response, socket.MSG_DONTROUTE, (addr[0], addr[1]))

    def v2gtp_handler(self, sock: socket.socket, mask: int) -> None:
        print(f"DBG >>>>> V2GTP Handler")
        # while until there are packets
        # buf = sock.recv(1024)
        # buf.parse()
        # sock.send(buf)

        print(">>>>>>>>>>> BEFORE")
        data_sock, addr = self.secc_sock.accept()
        print(">>>>>>>>>>> AFTER")
        # sock.setblocking(True)

        if mask & selectors.EVENT_READ:
            self.v2g_read(data_sock, mask)
            # data_socket, addr = self.secc_sock.accept()
            # sock, addr = self.secc_sock.accept()

            # self.selector.register(sock, selectors.EVENT_WRITE, data=self.v2g_read)

            # data = data_socket.recv(1024)
            # data = sock.recv(1024)
            # print(f"DBG >>>>> {data=}")
            # self.selector.modify(sock, selectors.EVENT_WRITE, data=self.v2gtp_handler)

        if mask & selectors.EVENT_WRITE:
            self.v2g_write(data_sock, mask)

    def v2g_read(self, sock, mask):
        self.log.debug("V2G Read")
        try:
            data = sock.recv(1024)
        except BlockingIOError:
            pass
        print(f"v2g_read: {data=}")
        # self.selector.modify(sock, selectors.EVENT_WRITE, data=self.v2gtp_handler)
        self.selector.register(sock, selectors.EVENT_WRITE, data=self.v2gtp_handler)

    def v2g_write(self, sock, mask):
        self.log.debug("V2G Write")
        try:
            sock.send(b"V2G WRITE FROM SERVER")
        except BlockingIOError:
            pass
        # self.selector.modify(sock, selectors.EVENT_READ, data=self.v2gtp_handler)
        self.selector.register(sock, selectors.EVENT_READ, data=self.v2gtp_handler)

    def start_server(self):
        try:
            while itertools.count(1):
                events = self.selector.select(timeout=None)
                for key, mask in events:
                    handler = key.data
                    handler(key.fileobj, mask)
        except KeyboardInterrupt:
            self.log.info("Caugth interrupt. Good bye.")
        finally:
            self.selector.close()


if __name__ == "__main__":
    srv = V2GTPServer(SDP_ADDR, SDP_PORT, ADDR, PORT)
    srv.start_server()
